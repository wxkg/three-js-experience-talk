# 智慧一张图

- 如果遇到技术问题，请联系管理员
- QQ：2354528292
- QQ扫描添加  ![image](./QQ.jpg)
- WX: aichitudousien
- WX扫描添加  ![image](./wx.jpg)
- 淘宝店铺: 爱吃土豆丝嗯
- 店铺扫描访问 ![image](./淘宝.png)

## 安装步骤

```
npm install
```

开发环境启动

```
npm run serve
```

### 打包命令

```
npm run build
```
