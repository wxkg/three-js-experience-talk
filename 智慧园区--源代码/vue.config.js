module.exports = {
  lintOnSave: false,
  devServer: {
    hot: false,
    liveReload: false,
    inline: false
  }
};
