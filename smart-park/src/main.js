import Vue from 'vue'
import App from './App.vue'
import ElementUI from 'element-ui';
import 'element-ui/lib/theme-chalk/index.css';
import router from './router'
import store from './store'
import './assets/css/reset.css';
import './assets/css/animate.css';
import './assets/css/style.css';
import bus from './bus'
import countTo from 'vue-count-to';
import dataV from '@jiaminghi/data-view';
import echarts from 'echarts'

Vue.component('count-to', countTo);
Vue.use(ElementUI);

Vue.config.productionTip = false;
Vue.prototype.$EventBus = bus;

Vue.prototype.$echarts = echarts;

Vue.use(dataV);

new Vue({
  router,
  store,
  render: h => h(App)
}).$mount('#app')
