// 模型材质设置
import * as THREE from 'three'
import { secondaryTexture, trunkTexture, primaryTexture, pointTexture } from './texture'; // 贴图

// 材质设置  ---- 模型模糊
export const floorBaseMaterial = new THREE.MeshBasicMaterial({
  color: 0x00beff, // 颜色
  transparent: true, // 透明
  opacity: 0.1,
  depthWrite: false, // 深度写入
})

// 材质设置  ---- 模型线性
export const floorBaseMaterial2 = new THREE.MeshBasicMaterial({
  color: 0x2590A1, // 颜色 0x+6进制
  transparent: true, // 透明
  opacity: 0.1,
  wireframe: true, // 将几何体渲染为线框。默认值为false（即渲染为平面多边形）。
})

// 黑色材质
export const darkMaterial = new THREE.MeshBasicMaterial({ color: 'black' });

// 地图线材质
export const secondaryMaterial = new THREE.MeshBasicMaterial({
  map: secondaryTexture,
  transparent: true,
  side: THREE.DoubleSide,
  opacity: 1
});
export const trunkMaterial = new THREE.MeshBasicMaterial({
  map: trunkTexture,
  transparent: true,
  side: THREE.DoubleSide,
  opacity: 1
});
export const primaryMaterial = new THREE.MeshBasicMaterial({
  map: primaryTexture,
  transparent: true,
  side: THREE.DoubleSide,
  opacity: 1
});

// 点材质
export const pointMaterial = new THREE.PointsMaterial({
  size: 2,
  map: pointTexture,
  fog: true,
  blending: THREE.AdditiveBlending,
  depthTest: false,
  transparent: true, // 透明
  opacity: 1 // 透明度
});
