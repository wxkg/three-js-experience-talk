// 文本

export function cssRender(cssRender, app) {
  const T = this
  T.config = {}
  T.init = function () {
    const cssRenderer = new cssRender();
    cssRenderer.setSize(app.el.offsetWidth, app.el.offsetHeight);
    cssRenderer.domElement.style.position = 'absolute';
    cssRenderer.domElement.style.top = 0;
    cssRenderer.domElement.style.pointerEvents = 'none';
    // cssRenderer.domElement._class = '_cssRender';
    app.el.appendChild(cssRenderer.domElement);
    T.cssRenderer = cssRenderer;
    T.cssRendererDomElement = cssRenderer.domElement;
  }

  // 文本添加
  T.add = function (option) {
    let list = []
    // 如果option是数组 直接赋值，否则添加
    if (Array.isArray(option)) list = option
    else list.push(option)

    list.forEach(e => {
      document.body.insertAdjacentHTML('beforeend', e.element); // 对应添加文本的HTML
      const label = new e.cssObject(document.body.lastChild);
      label.userData.isCss23D = true;
      label.position.set(...e.position);
      label.name = e.name;
      if (e.scale) label.scale.set(...e.scale); //设置缩放
      e.parent ? e.parent.add(label) : app.scene.add(label);
      T.config[e.name] = label;
    })
  }

  // 删除所有文本（需要使用倒序删除文本）
  T.removeAll = function(parent) {
    // 需要倒序遍历
    for (let i = parent.children.length - 1; i >= 0; i--) {
      const e = parent.children[i];
      if (e.userData.isCss23D) {
        const name = e.name;
        parent.remove(e);
        if (T.config[name]) delete T.config[name];
      }
    }
  };

  T.init(); // 启动文本
}

