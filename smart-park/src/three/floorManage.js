// 楼层管理-------

// 飞行动画
// app.instance文本实例

// import * as THREE from 'three'
import {
  CSS3DSprite
} from 'three/examples/jsm/renderers/CSS3DRenderer'; // 文本系列
import {
  floorBaseMaterial
} from './material'; // 文本系列
import EventBus from '@/bus'// 监听两个页面的数据，传参
import { destroyControlGroupText } from '@/three/loaderModel'; // 统一处理所有文本，清空控制器组的文本
import { roomTexts } from '@/assets/mock/mock'; // 楼层显示的文本 摄像头等

export function loaderFloorManage(app) {
  // 要飞行的xyz轴
  // app.camera.position -4.67, 137.06, 117.29
  // app.contrils.target -1.21, 1.17, -1.31
  app.flyTo({
    position: [-4.67, 137.06, 117.29],
    controls: [-1.21, 1.17, -1.31],
    done: () => {
      console.log('相机飞行完成')
      // 文本相关
      createFloorText(app) // 楼顶文本
    }
  })
}

// 楼顶文本设置
export function createFloorText(app) {
  app.model.traverse(obj => { // 递归循环
    if (obj.name.indexOf('楼顶') > -1) {
      console.log(obj)
      const name = obj.parent.name
      const position = Object.values(app.getModelWorldPostion(obj));
      console.log(name, position)
      const html = `<div class="floorText-3d animated fadeIn"  id="${name}" position="${position}"><p class="text">${name}</p></div>`
      app.instance.add({
        parent: app.controlGroup, // 父级
        element: html, // div
        position: position, // 位置
        cssObject: CSS3DSprite,
        name, // 名字
        scale: [0.05, 0.05, 0.05] // 缩放
      })
    }
  })

  //点击某一个楼
  const textDoms = document.getElementsByClassName('floorText-3d'); // 获取所有的dom
  for (let i = 0; i < textDoms.length; i++) {
    const textDom = textDoms[i];
    textDom.onclick = () => {
      for (let i = 0; i < app.model.children.length; i++) {
        const obj = app.model.children[i];
        if (obj.name === textDom.id) {
          // 显示返回文本和楼层ui
          EventBus.$emit('changeFloorUI', {
            isShowFloorBack: true,
            model: obj
          });
          // 计算当前点击模型的中心点---世界坐标
          const centerPosition = Object.values(app.getModelWorldPostion(obj)); // Object.values() 返回一个数组(对象转数组)
          // 相机的飞行动画
          app.flyTo({
            position: [centerPosition[0] + 40, centerPosition[1] + 50, centerPosition[2] + 40],
            controls: centerPosition // 看向的点
          });
          // 恢复点击模型组的材质
          obj.traverse(childrenObj => {
            if (childrenObj.material) {
              childrenObj.material = app.modelMaterials[childrenObj.name].material;
            }
          });
        } else {
          // 设置除点击模型的组以外的基础色，材质颜色
          obj.traverse(childrenObj => {
            if (childrenObj.material) {
              childrenObj.material = floorBaseMaterial; // 模糊材质
            }
          });
        }
      }

      destroyControlGroupText(app, 'floorText-3d'); // 统一处理所有文本，清空控制器组的文本
    };
  }
}

// 创建当前楼层的icon，生成摄像头、房间文本和绑定点击事件
export function createRoomText(app, model) {
  console.log('生成摄像头、房间文本', model)
  model.traverse((obj) => {
    if (obj.isMesh) { // 判断是否存在网格模型
      roomTexts.forEach(item => {
        if (obj.name.indexOf(item.name) > -1) {
          console.log('当前楼层存在的icon,摄像头等', obj.name);
          const name = obj.name;
          const position = Object.values(app.getModelWorldPostion(obj)); // 找到当前模型的世界坐标
          position[0] += item.x;
          position[1] += item.y;
          position[2] += item.z;
          // room-3d icon的class
          const html = `
        <div class="room-3d animated fadeIn"  _type="${item.type}"  id="${name}" position="${position}" >
          <p class="text">${name}</p>
          <div class="${item.class}"></div>
        </div>`;
          app.instance.add({
            parent: app.controlGroup, // 当前的控制器
            cssObject: CSS3DSprite, // CSS3DRenderer用于通过CSS3的transform属性， 将层级的3D变换应用到DOM元素上。
            name: name,
            element: html,
            position: position,
            scale: [0.01, 0.01, 0.01]
          });
        }
      });
    }
  });

  // 点击事件-------icon点击
  const textDoms = document.getElementsByClassName('room-3d'); // 获取所有的icon
  for (let i = 0; i < textDoms.length; i++) {
    const textDom = textDoms[i];
    textDom.onclick = (event) => {
      const type = textDom.getAttribute('_type'); // 获取自定义对象的属性访问行为
      const model = app.model.getObjectByName(textDom.id);
      EventBus.$emit('changeRoomTooltip', {
        name: model.name,
        type,
        x: event.x, // 鼠标点击的x
        y: event.y,
        show: true, // 是否显示
      });
    };
  }
}

// 设置当前楼层动画函数，处理创建楼层  model:点击楼栋 layerName：当前点击楼层名称 layerData：当前点击楼层信息 callback：回调函数
export function setModelLayer(app, model, layerName, layerData, callback) {
  destroyControlGroupText(app, 'room-3d');  // 清除当前楼层文本
  const currentLayer = Number(layerName.substr(0, layerName.indexOf('F'))) // 当前的楼层
  for (let i = 0; i < model.children.length; i++) {
    let mesh = model.children[i];
    let name = mesh.name
    let num;

    // 对楼顶进行特殊处理
    if (name.indexOf('楼顶') > -1) {
      num = layerData.length + 1 // 楼顶 楼层+1
    } else {
      num = Number(name.substr(0, name.indexOf('F')))
    }

    let value = num - currentLayer
    let position = mesh.position // 当前点击楼层的位置,起点坐标
    let position_tmp = mesh.position_tmp // 备份模型位置
    let toPosition; // 结束坐标，终点坐标
    console.log('postion_tmp', position_tmp)

    if (layerName === '全楼') {
      toPosition = [position_tmp.x, position_tmp.y, position_tmp.z]
    } else {
      if (value >= 1) {
        // 楼层之间相差大于1
        toPosition = [position_tmp.x, position_tmp.y + value * 20, position_tmp.z]
      } else {
        toPosition = [position_tmp.x, position_tmp.y, position_tmp.z]
      }
    }

    // 模型移动
    app.modelMove({
      fromPosition: [position.x, position.y, position.z], // 初始位置
      toPosition, // 结束的位置
      duration: 300,
      done:() => {
        if (layerName === '全楼') {
          if (callback) {
            callback();
            console.log('callback()',callback())
            return;
          }
          // 点击当前楼层 缩进放大当前楼层  拉近视觉距离
          const centerPosition = Object.values(app.getModelWorldPostion(model)); //  Object.values()组成一个数组返回
            app.flyTo({
              position: [centerPosition[0] + 40, centerPosition[1] + 50, centerPosition[2] + 40],
              controls: centerPosition
            });
            return;
        } else {
          // mesh代表当前点击的楼层
          if (mesh.name.indexOf(layerName) > -1) {
            if (callback) {
              callback();
              return;
            }
           // 计算当前点击模型的中心点
           const centerPosition = Object.values(app.getCalculationPostion(mesh));
            app.flyTo({
              position: [centerPosition[0] + 15, centerPosition[1] + 20, centerPosition[2] + 15],
              controls: centerPosition,
              done: () => {
                createRoomText(app, mesh); // 创建当前楼层的一些展示的icon app：整个模型  mesh: 当前的模型
              }
            });
          }
        }
      }
    }, mesh) // 传入模型mesh
  }
}