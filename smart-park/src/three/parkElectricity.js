// 园区----电力检测

// import * as THREE from 'three';
import { floorBaseMaterial2 } from './material';
import { parkData } from '@/assets/mock/mock'; // 数据
import { Notification } from 'element-ui'; // 通知
import EventBus from '../bus'; // 页面传参
import { setModelDefaultMatrial } from '@/three/loaderModel'; // 设置模型---默认材质

// 设置电力监测的视角
export function loaderParkElectricity(app) {
  app.flyTo({
    position: [53, 68.1, 132.1],
    controls: [-3.85, -12.06, 8.96],
    done: () => {
      createElectricityModel(app);
    }
  });
}

export function createElectricityModel(app) {
  let notifIndex = 0;
  app.model.traverse((obj) => {
    if (obj.isMesh) { // 判断模型是否存在
      if (obj.name.indexOf('电表') === -1) {
        obj.material = floorBaseMaterial2;
      } else {
        // 获取到每一个电表的值
        const floorName = obj.parent.parent.name; // 楼栋名字
        const layerName = obj.parent.name.substr(0, 2); // 楼层名字
        const roomName = obj.name.substr(0, 3); // 房间的名字
        const value = parkData[floorName][layerName][roomName]['电'];
        // 定义阈值--超出报警
        if (value > 460) {
          app.selectedObjects.push(obj); // 把超出度数的电表添加到selectedObjects，用于改变模型的外表使其发光
          if (notifIndex < 6) { // 最多弹六个
            setTimeout(() => {
              Notification({
                title: '警告',
                message: `${floorName}的${layerName}${roomName}的用电量为${value}度, 已超过平均标准，请留意。`,
                type: 'warning',
                duration: 6000
              });
            }, notifIndex * 200);
            notifIndex++;
          }
          app.rayModel.push(obj); // 把超出的模型添加到射线数组中--电表的模型数组，用于点击事件
        }
      }
    }
  });

  // 电表--射线 弹窗
  app.initRaycaster((activeObj, app, event) => {
      if (activeObj.object) {
        const obj = activeObj.object;
        const floorName = obj.parent.parent.name;
        const layerName = obj.parent.name.substr(0, 2);
        const roomName = obj.name.substr(0, 3);
        const value = parkData[floorName][layerName][roomName]['电'];
        EventBus.$emit('changeTooltip', {
          楼栋: floorName,
          楼层: layerName,
          房间号: roomName,
          度数: value,
          name: obj.name,
          type: '电',
          x: event.x,
          y: event.y,
          show: true
        });
      } else {
        EventBus.$emit('changeTooltip', {
          show: false
        });
      }
    },
    app.rayModel,
    'click'
  );
}

// 清除线型模型效果
export function destroyParkElectricity(app) {
  app.selectedObjects = []; // 消除模型边缘高亮
  app.outlinePass.selectedObjects = app.selectedObjects;
  EventBus.$emit('changeTooltip', {
    show: false
  });
  setModelDefaultMatrial(app); // 设置模型---默认材质
  app.destroyRaycaster('click'); // 销毁一下射线 --点击事件
}
