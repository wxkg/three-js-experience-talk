import * as THREE from 'three';
import {
  GUI
} from 'three/examples/jsm/libs/lil-gui.module.min.js'; // 颜色转换器
import {
  OrbitControls
} from 'three/examples/jsm/controls/OrbitControls' // 轨道控制器
import {
  DRACOLoader
} from 'three/examples/jsm/loaders/DRACOLoader' // 加载压缩
import {
  OBJLoader
} from 'three/examples/jsm/loaders/OBJLoader'; // 模型加载器：obj加载器
import {
  MTLLoader
} from 'three/examples/jsm/loaders/MTLLoader'; // 模型加载器：mtll加载器
import {
  GLTFLoader
} from 'three/examples/jsm/loaders/GLTFLoader' //  模型加载器：gltf、glb加载器
import {
  FBXLoader
} from 'three/examples/jsm/loaders/FBXLoader'; // 模型加载器：fbx加载器
import {
  RGBELoader
} from 'three/examples/jsm/loaders/RGBELoader.js'; // 加载环境贴图
import TWEEN from 'three/examples/jsm/libs/tween.module.js'; // 飞行动画

export default class zThree {
  constructor(id) {
    this.id = id;
    this.el = document.getElementById(id) // 当前整个模型
  }

  // 初始化场景
  initThree() {
    let that = this;
    let width = this.el.offsetWidth;
    let height = this.el.offsetHeight;
    this.scene = new THREE.Scene() // 创建场景
    this.textureLoader = new THREE.TextureLoader() // 加载贴图的
    this.camera = new THREE.PerspectiveCamera(45, width / height, 1, 1000) // 相机
    this.camera.position.set(30, 30, 30) // 相机的位置
    this.camera.lookAt(0, 0, 0) // 计算相机视线方向
    this.renderer = new THREE.WebGLRenderer({
      antialias: false, // 是否开启抗锯齿
      alpha: true // 是否透明
    })
    this.renderer.setPixelRatio(window.devicePixelRatio) // 设置设备像素比
    this.renderer.setSize(width, height)  // 设置渲染区域尺寸
    this.el.append(this.renderer.domElement) // body元素中插入Threejs渲染结果：canvas对象

    this.gui = new GUI() // 颜色工具

    // 屏幕自适应
    window.addEventListener('resize', () => {
      that.camera.aspect = that.el.offsetWidth / that.el.offsetHeight // 摄像机视锥体长宽比
      that.camera.updateProjectionMatrix() // .fov、.aspect、.near、.far属性发生改变之后，都需要调用.updateProjectionMatrix()来使得这些改变生效
      that.renderer.setSize(that.el.offsetWidth / that.el.offsetHeight)// 重新渲染区域尺寸
      // 文本渲染器
      if (that.cssRenderer) {
        that.cssRenderer.setSize(that.el.offsetWidth / that.el.offsetHeight)
      }
    })
  }

  // 初始化射线
  initRaycaster(callback, models = this.scene.children, eventName = 'click') {
    this.raycaster = new THREE.Raycaster(); // 射线拾取
    this.raycaster.params.Line.threshold = 0.01 // 设置射线的拾取的精确度为0.01
    this.rayFn = this.rayEventFn.bind(this, models, callback); // 绑定一个点击事件
    // 绑定点击事件
    this.el.addEventListener(eventName, this.rayFn);
  }

  // 点击射线
  rayEventFn(models, callback) {
    let evt = window.event; // e || window.event、事件注册    e.srcElement    e.target
    // 当前坐标offsetX 和offsetY
    let mouse = {
      x: (evt.clientX / window.innerWidth) * 2 - 1,
      y: -(evt.clientY / window.innerHeight) * 2 + 1
    };

    let activeObj = this.fireRaycaster(mouse, models);
    // if (activeObj.point) {
    //   console.log([activeObj.point.x, activeObj.point.y, activeObj.point.z]);
    //   console.log(activeObj);
    // }
    if (callback) {
      // activeObj:点击获取到的物体, this:当前的this, evt:当前点击事件的对象, mouse:点
      callback(activeObj, this, evt, mouse);
    }

    // 鼠标的变换
    document.body.style.cursor = 'pointer';
  }

  // 射线---返回选中物体
  fireRaycaster(pointer, models) {
    // 使用一个新的原点和方向来更新射线
    this.raycaster.setFromCamera(pointer, this.camera);
    // 点击射线之后获取到物体的数组
    let intersects = this.raycaster.intersectObjects(models, true);
    // 判断是否点击到物体
    if (intersects.length > 0) {
      let selectedObject = intersects[0];
      return selectedObject;
    } else {
      return false;
    }
  }

  // 销毁一下射线 --- 点击事件
  destroyRaycaster(eventName) {
    this.raycaster = null; // 射线重新赋值
    this.el.removeEventListener(eventName, this.rayFn);
  }

  // 光源
  initLight() {
    // 环境光源
    const ambientLight = new THREE.AmbientLight(0xffffff, 0.7); // 柔和的白光
    this.scene.add(ambientLight);
    const folderAmbient = this.gui.addFolder('环境光源');
    folderAmbient.close();
    folderAmbient.addColor(ambientLight, 'color');
    folderAmbient.add(ambientLight, 'intensity', 0, 2.0, 0.1);

    // 从上方照射的白色平行光，强度为 0.5。
    const directionalLight = new THREE.DirectionalLight('#3e9ae0', 0.7);
    directionalLight.position.set(-100, 100, -100);
    directionalLight.castShadow = true; // 影子的对象设置
    this.scene.add(directionalLight);

    const folderDirectional = this.gui.addFolder('平行光');
    folderDirectional.close();
    folderDirectional.addColor(directionalLight, 'color');
    folderDirectional.add(directionalLight, 'intensity', 0, 2.0, 0.1);

    // 设置灯光的阴影属性
    directionalLight.shadow.mapSize.width = 1024;
    directionalLight.shadow.mapSize.height = 1024;
    directionalLight.shadow.camera.near = 10; // 近
    directionalLight.shadow.camera.far = 300; // 远
    directionalLight.shadow.camera.top = 100; // 上
    directionalLight.shadow.camera.bottom = -100; // 下
    directionalLight.shadow.camera.left = -100; // 左
    directionalLight.shadow.camera.right = 100; // 右
    directionalLight.shadow.bias = 0.05; // 阴影偏移
    directionalLight.shadow.normalBias = 0.05; // 阴影偏移

    // 平行光的辅助对象---相机辅助-----相机线轴
    // const helper = new THREE.CameraHelper(directionalLight.shadow.camera);
    // this.scene.add(helper);

    // gui工具是否展示
    this.gui.hide();
  }

  // 辅助的坐标抽
  initHelper() {
    this.scene.add(new THREE.AxesHelper(100))
  }

  // 初始化轨道控制器
  initOrbitControls() {
    let controls = new OrbitControls(this.camera, this.renderer.domElement)
    controls.enableDamping = true // 阻尼（惯性），这将给控制器带来重量感，必须在你的动画循环里调用.update()。
    controls.enableZoom = true; // 是否开启缩放
    controls.autoRotate = false // 是否开启模型旋转
    controls.autoRotateSpeed = 0.5 // 旋转的速度
    controls.enablePan = true // 是否摄像头平移

    this.controls = controls
  }

  // 加载模型
  loaderModel(option) {
    switch (option.type) {
      case 'obj':
        if (!this.objLoader) {
          this.objLoader = new OBJLoader();
        }
        if (!this.mtlLoader) {
          this.mtlLoader = new MTLLoader();
        }
        this.mtlLoader.load(option.mtlUrl || '', (materials) => {
          materials.preload();
          this.objLoader
            .setMaterials(materials)
            .load(option.url, option.onLoad, option.onProgress, option.onError);
        });
        break;

      case 'gltf':
      case 'glb':
        if (!this.gltfLoder) {
          this.gltfLoder = new GLTFLoader(); // 模型加载器：gltf、glb加载器
          let dracoLoader = new DRACOLoader(); // 模型压缩
          dracoLoader.setDecoderPath('draco/') // blender导出--点击压缩就必须用这个压缩的api
          this.gltfLoder.setDRACOLoader(dracoLoader) // 引入模型压缩
        }
        // 模型加载 （模型地址，模型加载成功的回调函数，模型加载进度，模型报错函数）
        this.gltfLoder.load(option.url, option.onLoad, option.onProgress, option.onError)
        break;
      case 'fbx':
        if (!this.fbxLoader) {
          this.fbxLoader = new FBXLoader();
        }
        this.fbxLoader.load(option.url, option.onLoad, option.onProgress, option.onError);
        break;

      case 'rgbe':
        if (!this.rgbeLoader) {
          this.rgbeLoader = new RGBELoader();
        }
        this.rgbeLoader.load(option.url, option.onLoad, option.onProgress, option.onError);
        break;

      case 'mp3':
      case 'wav':
        if (!this.audioaLoader) {
          this.audioaLoader = new THREE.AudioLoader();
        }
        this.audioaLoader.load(option.url, option.onLoad, option.onProgress, option.onError);
        break;

      default:
        console.error('当前只支持obj, gltf, glb, fbx, rgbe格式');
        break;
    }
  }

  // 迭代加载
  iterateLoad(objFileList, onProgress, onAllLoad) {
		let fileIndex = 0;
		let that = this;

		function iterateLoadForIt() {
			that.loaderModel({
				type: objFileList[fileIndex].type,
				dracoUrl: objFileList[fileIndex].dracoUrl,
				mtlUrl: objFileList[fileIndex].mtlUrl,
				url: objFileList[fileIndex].url,
				onLoad: function(object) {
					if (objFileList[fileIndex].onLoad) objFileList[fileIndex].onLoad(object);
					fileIndex++;
					if (fileIndex < objFileList.length) {
						iterateLoadForIt();
					} else {
						if (onAllLoad) onAllLoad();
					}
				},
				onProgress: function(xhr) {
					if (objFileList[fileIndex].onProgress) objFileList[fileIndex].onProgress(xhr,
						fileIndex);
					if (onProgress) onProgress(xhr, fileIndex);
				}
			});
		}
		iterateLoadForIt();
	}

  // 加载天空盒
  loaderSky(path) {
    let skyTexture = new THREE.CubeTextureLoader().setPath(path).load([
      'px.jpg', // 右
      'nx.jpg', // 左
      'py.jpg', // 上
      'ny.jpg', // 下
      'pz.jpg', // 前
      'nz.jpg' // 后
    ]);
    return skyTexture;
  }

  // 获取世界坐标
  getModelWorldPostion(model) {
    this.scene.updateMatrixWorld(true);  // 更新场景的矩阵
    const worldPosition = new THREE.Vector3(); // 三维向量Vector3与模型位置
    model.getWorldPosition(worldPosition); // 获取三维场景中model在世界坐标下的三维坐标
    return worldPosition;
  }

  // 自动计算物体飞行点
  getCalculationPostion(object) {
    let Box = new THREE.Box3();
    Box.setFromObject(object);
    if (Box.isEmpty()) return;
    let min = Box.min;
    let max = Box.max;
    // let width = max.x - min.x;
    // let height = max.y - min.y;
    // let deepth = max.z - min.z;
    // 获取绝对点坐标
    let centroid = new THREE.Vector3();
    centroid.addVectors(min, max);
    centroid.multiplyScalar(0.5);
    return centroid;
  }

  // 相机飞行视角
  flyTo(option) {
    option.position = option.position || []; // 相机新的位置
    option.controls = option.controls || []; // 控制器新的中心点位置(围绕此点旋转等)
    option.duration = option.duration || 1000; // 飞行时间
    option.easing = option.easing || TWEEN.Easing.Linear.None; // three.js 缓动算法.easing(渐入相机动画)
    const curPosition = this.camera.position;
    const controlsTar = this.controls.target;

    // 目标点从第一个点到第二个点
    const tween = new TWEEN.Tween({
      x1: curPosition.x, // 相机当前位置x
      y1: curPosition.y, // 相机当前位置y
      z1: curPosition.z, // 相机当前位置z
      x2: controlsTar.x, // 控制当前的中心点x
      y2: controlsTar.y, // 控制当前的中心点y
      z2: controlsTar.z // 控制当前的中心点z
    }).to({
      x1: option.position[0], // 新的相机位置x
      y1: option.position[1], // 新的相机位置y
      z1: option.position[2], // 新的相机位置z
      x2: option.controls[0], // 新的控制中心点位置x
      y2: option.controls[1], // 新的控制中心点位置x
      z2: option.controls[2] // 新的控制中心点位置x
    }, option.duration).easing(option.easing) // TWEEN.Easing.Cubic.InOut //匀速

    // 开始
    tween.onStart(() => {
      // 开始后controls不允许在此操作
      this.controls.enabled = false
      // 判断一下option.satrt回调函数是否存在
      if (option.start instanceof Function) {
        option.start()
      }
    })
    // 更新
    tween.onUpdate(() => {
      // 开始后controls不允许在此操作
      this.controls.enabled = false
      // 设置新的位置
      this.camera.position.set(tween._object.x1, tween._object.y1, tween._object.z1);
      this.controls.target.set(tween._object.x2, tween._object.y2, tween._object.z2);
      // 判断一下option.update是否存在
      if (option.update instanceof Function) {
        option.update()
      }
    })
    // 完成
    tween.onComplete(() => {
      // 开始后controls不允许在此操作
      this.controls.enabled = true // 是否可以滑动
      // 判断一下option.satrt是否存在
      if (option.done instanceof Function) {
        option.done()
      }
    })
    // 暂停
    tween.onStop(() => {
      // 开始后controls不允许在此操作
      this.controls.enabled = false
      // 判断一下option.satrt是否存在
      if (option.stop instanceof Function) {
        option.stop()
      }
    })
    tween.start() // 调用开始
    TWEEN.add(tween);
    return tween;

  }


  // 模型移动
  modelMove(option, obj) {
    option.fromPosition = option.fromPosition || []; // 初始位置
    option.toPosition = option.toPosition || []; // 结束位置

    option.duration = option.duration || 1000; // 飞行时间
    option.easing = option.easing || TWEEN.Easing.Linear.None;
    let tween = new TWEEN.Tween({
      x1: option.fromPosition[0],
      y1: option.fromPosition[1],
      z1: option.fromPosition[2]
    })
      .to(
        {
          x1: option.toPosition[0],
          y1: option.toPosition[1],
          z1: option.toPosition[2]
        },
        option.duration
      )
      .easing(TWEEN.Easing.Linear.None);
    tween.onUpdate(() => {
      this.controls.enabled = false;
      obj.position.set(tween._object.x1, tween._object.y1, tween._object.z1);

      this.controls.update();
      if (option.update instanceof Function) {
        option.update(tween);
      }
    });
    tween.onStart(() => {
      this.controls.enabled = false;
      if (option.start instanceof Function) {
        option.start();
      }
    });
    tween.onComplete(() => {
      this.controls.enabled = true;
      if (option.done instanceof Function) {
        option.done();
      }
    });
    tween.onStop(() => (option.stop instanceof Function ? option.stop() : ''));
    tween.start();
    TWEEN.add(tween);
    return tween;
  }

  // 渲染
  render(callabck) {
    callabck();
    this.frameId = requestAnimationFrame(() => this.render(callabck))
  }

}