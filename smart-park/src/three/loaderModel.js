import * as THREE from 'three'
import EventBus from '../bus'
import { secondaryMaterial, trunkMaterial, primaryMaterial } from './material'

export function loaderModel(app) {
	return new Promise((resolve) => {
		app.controlGroup = new THREE.Group() // 模型控制组，创建一个组对象 方便删除创建
		app.modelMaterials = {} // 存储模型的所有材质
		app.rayModel = [] // 射线的数组--用于点击事件
		app.scene.add(app.controlGroup)
		const urls = [
			// 贴图--增加玻璃效果
			{
				type: 'rgbe',
				url: 'texture/royal_esplanade_1k.hdr', // 基于publice文件夹下
				onLoad: (texture) => {
					// 模型表面增加贴图
					const pmremGenerator = new THREE.PMREMGenerator(app.renderer)
					pmremGenerator.compileEquirectangularShader()
					app.envMap = pmremGenerator.fromEquirectangular(texture).texture // 给模型的envMap进行贴图
				},
			},
			{
				type: 'glb',
				url: 'model/model.glb',
				onLoad: (object) => {
					// 成功加载模型的回调函数
					console.log('成功加载模型的回调函数----', object)
					app.scene.add(object.scene) // 模型加载场景

					// 设置模型接受阴影和不接受阴影的
					app.model = object.scene
					const receiveModel = ['平面017_1', '平面017_2', '平面017_3', '平面017_4'] // 哪些物体有投影
					const bloomModels = ['楼间隔', '承重柱', '玻璃', '路灯面', '停车线', '顶亮面', '车流线'] // 判断哪些放到辉光哪些不放，改变模型动态效果的
					// Traverse 是 Three.js 中的一个方法，它的主要作用是对场景中的物体进行深度优先遍历。
					// 通过 Traverse，我们可以获取场景中的所有物体，并对这些物体进行操作，如添加、删除、修改等。

					app.model.traverse((obj) => {
						// 判断模型材质
						// 将所有模型的材质储存在app.modelMaterials，在后续改变材质使用
						if (obj.material) {
							// 拿到所有材质
							app.modelMaterials[obj.name] = {
								material: obj.material,
							}
							if (obj.name == '其余建筑_1') {
								obj.material.envMap = app.envMap
							}
						}
						// 记录一下楼层初始的位置
						let { x, y, z } = obj.position
						obj.position_tmp = { x, y, z } // 将模型的坐标也储存一份，在做楼层动画使用

						// 接收投影设置
						if (receiveModel.includes(obj.name)) {
							obj.receiveShadow = true // 设置材质是否接受阴影
						} else {
							obj.castShadow = true // 设置光照投射阴影
							obj.receiveShadow = false
						}

						if (obj.name.indexOf('车流线1') > -1) {
							obj.material = secondaryMaterial // 绿色
						}

						if (obj.name.indexOf('车流线2') > -1) {
							obj.material = trunkMaterial // 红色
						}

						if (obj.name.indexOf('车流线3') > -1) {
							obj.material = primaryMaterial // 蓝色
						}

						// 判断哪些需要添加到辉光，进行分层
						for (let i = 0; i < bloomModels.length; i++) {
							const value = bloomModels[i]
							if (obj.name.indexOf(value) > -1) {
								obj.layers.enable(1) //添加到1层里 layers图层对象
								break
							} else {
								obj.layers.enable(0) // 添加到0层里
							}
						}
					})
				},
			},
		]

		// 调用迭代加载模型的方法
		let urlsLength = urls.length
		console.log('urlsLength', urlsLength)
		app.iterateLoad(
			urls,
			(xhr) => {
				let proportion = parseInt((xhr.loaded / xhr.total) * 100)
				if (proportion === 100) {
					urlsLength = 1 // 服务器和本地有差异 服务器需注掉这一行
					EventBus.$emit('changeLoaidng', parseInt(100 / urlsLength))
					console.log(parseInt(100 / urlsLength), 'sadasdsa')
					urlsLength--
					if (urlsLength <= 1) {
						EventBus.$emit('changeScene', true)
					}
				}
			},
			() => {
				app.scene.add(app.model)
				resolve()
			},
		)
	})
}

// 设置模型---默认材质
export function setModelDefaultMatrial(app) {
	app.model.traverse((obj) => {
		if (obj.material) {
			obj.material = app.modelMaterials[obj.name].material // 之前建的组存储的材质
		}
	})
}

// 删除文本--------------统一处理所有文本，清空控制器组的文本
export function destroyControlGroupText(app, className) {
	const textDoms = document.getElementsByClassName(className) // 获取所有dom
	for (let i = 0; i < textDoms.length; i++) {
		const textDom = textDoms[i]
		textDom.onclick = null
	}
	app.instance.removeAll(app.controlGroup) // 删除控制器组（文本）
}

// 删除模型 ---------------
export function destroyControlGroup(app, className) {
	// 判断是否存在controlGroup
	if (app?.controlGroup?.children?.length === 0) {
		return
	}
	if (className) {
		destroyControlGroupText(app, className)
	}

	// 倒序删除
	for (let i = app.controlGroup.children.length - 1; i > -1; i--) {
		const obj = app.controlGroup.children[i]
		if (obj.isMesh) {
			obj.geometry.dispose() // 模型销毁
			obj.material.dispose() // 材质销毁
			app.controlGroup.remove(obj) // 组里移除
		}
	}
}
