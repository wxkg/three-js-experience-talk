import { defineConfig } from 'vite'
import vue from '@vitejs/plugin-vue'
import path from 'path'

// https://vitejs.dev/config/
export default defineConfig({
  base: './', // 这个是给每个资源，设置一个跟路径，这个是在生产环境下才会由效果的。这个可以解决在项目部署时，有些资源访问不到的问题。
  plugins: [vue()],
  server: {
    host: 'localhost',
    port: 9999,
    open: true, //启动项目时，在浏览器中自动打开程序
    hmr: true, // 禁止或者开启hmr连接
    // 传递给 chockidar 的文件系统监视器选项
    watch: {
      ignored: ['!**/node_modules/your-package-name/**'],
    },
    https: false, // 是否开启 https,
    proxy: {
      '/api': {
        target: 'http://localhost:3000',
        changeOrigin: true,
        rewrite: (path) => path.replace(/^\/api/, ''),
      },
    },
  },
  resolve: {
    alias: [
      {
        find: '@',
        replacement: path.resolve(__dirname, 'src'),
      },
      {
        find: 'vites',
        replacement: path.resolve(__dirname, 'src/view'),
      },
    ],
  },
  build: {
    // 生产环境下清空console.log
    minify: 'terser',
    terserOptions: {
      compress: {
        // 打包自动删除console
        drop_console: true,
        drop_debugger: true,
      },
      keep_classnames: true,
    },
    // 打包出的文件整理
    rollupOptions: {
      output: {
        chunkFileNames: 'js/[name]-[hash].js',
        entryFileNames: 'js/[name]-[hash].js',
        assetFileNames: '[ext]/[name]-[hash].[ext]',
      },
    },
  },
})
