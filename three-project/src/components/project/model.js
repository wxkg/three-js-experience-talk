import * as THREE from 'three'
import { nextTick } from 'vue'

// 引入gltf加载器
import {
  GLTFLoader
} from 'three/addons/loaders/GLTFLoader.js'

// 实例化一个加载器对象
const loader = new GLTFLoader() // 创建一个GTF加载器

const model = new THREE.Group() // 声明一个组对象，用来添加加载成功的三维场景建一个模型

nextTick(() => {
  var percentDiv = document.getElementById("per"); // 获取进度条元素
  loader.load('../../public/工厂.gltf', gltf => {
    // console.log(gltf)
    model.add(gltf.scene) // 模型增加gltf的场景

   

    // 加载完成，隐藏进度条
    // document.getElementById("container").style.visibility ='hidden';
    document.getElementById("container").style.display = 'none';

  }, xhr => {
    // 控制台查看加载进度xhr
    // 通过加载进度xhr可以控制前端进度条进度   
    const percent = xhr.loaded / xhr.total;
    console.log('加载进度' + percent);
    // 进度条加载
    percentDiv.style.width = percent * 400 + "px"; //进度条元素长度
    percentDiv.style.textIndent = percent * 400 + 5 + "px"; //缩进元素中的首行文本
    // Math.floor:小数加载进度取整
    percentDiv.innerHTML = Math.floor(percent * 100) + '%'; //进度百分比

  })
});



export default model