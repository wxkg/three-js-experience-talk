import * as THREE from 'three';
import { OrbitControls } from 'three/addons/controls/OrbitControls.js';

import  model  from './model.js';//模型对象

//场景
const scene = new THREE.Scene();
// model.rotateY(Math.PI / 4); // .rotateY()默认绕几何体中心旋转，经过上面几何体平移变化，你会发现.rotateY()是绕长方体面上一条线旋转
model.rotateY(0.01);//旋转动画
requestAnimationFrame(render);
scene.add(model); //模型对象添加到场景中


// 辅助观察的坐标系轴线-----0-100，0
const axesHelper = new THREE.AxesHelper(0);
scene.add(axesHelper);


//光源设置
const directionalLight = new THREE.DirectionalLight(0xffffff, 0.8);
directionalLight.position.set(400, 200, 300);
scene.add(directionalLight);
const ambient = new THREE.AmbientLight(0xffffff, 0.4);
scene.add(ambient);


//渲染器和相机（加载模型角度有问题调它）
const width = window.innerWidth;
const height = window.innerHeight;
const camera = new THREE.PerspectiveCamera(30, width / height, 1, 3000);
camera.position.set(-292, 223, 185); // 相机位置，根据渲染范围尺寸数量级设置相机位置
camera.lookAt(0, 0, 0); // 相机视线指向原点（想让哪个位置在中心点就用谁的坐标）,当这个改变的时候，需要配合controls.targert使用，也要改

// 创建一个渲染器
const renderer = new THREE.WebGLRenderer({
    antialias: true, // 开启优化锯齿
    alpha: false, // true的时候渲染器的背景为透明
    preserveDrawingBuffer: true, // 如果想把canvas的内容下载到本地，需要设置为true
    logarithmicDepthBuffer: true,// 设置对数深度缓冲区，优化深度冲突问题
});
renderer.setPixelRatio(window.devicePixelRatio) // 防止渲染模糊
renderer.setSize(width, height);
renderer.outputEncoding = THREE.sRGBEncoding; //解决加载gltf格式模型颜色偏差问题， 提醒：有的版本这里不用在设置，默认就是sRGB
renderer.setClearColor(0x444444, 1) // 设置渲染器背景色和透明度
// 设置canvas的position属性（需要叠加到canvas上）
// renderer.domElement.style.position = 'absolute'
// renderer.domElement.style.top = '0px'
// renderer.domElement.style.left = '0px'
// renderer.domElement.style.zIndex = '-1'
renderer.setClearAlpha(1) // 渲染器背景透明度0-1


// document.body.appendChild(renderer.domElement);


// 通过OrbitControls设置相机位置.position
const controls = new OrbitControls(camera, renderer.domElement); // 放到渲染前边
controls.target.set(0,0,0) // 与lookAt参数保持一致
controls.update()

// 渲染循环
function render() {
    // console.log('controls.target', controls.target)
    renderer.render(scene, camera);
    requestAnimationFrame(render);
}
render();



// 画布跟随窗口变化
window.onresize = function () {
    // 重置渲染器在canvas的宽高度，设置three输出的宽高
    renderer.setSize(window.innerWidth, window.innerHeight);
    // 全屏情况下：这是观察范围长宽比aspect为窗口的宽高比
    camera.aspect = window.innerWidth / window.innerHeight;
    // 当相机的一些属性发生变化需执行下列操作
    camera.updateProjectionMatrix();
};

export default renderer